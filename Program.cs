﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace krestikiNoliki
{
    class Program
    {
        enum cell
        {
            zero,
            krest,
            empty
        }
        static void Main(string[] args)
        {
            cell[,] a = new cell[3, 3];
            int stroka;
            int stolbec;
            int step = 0;
            bool playGame = true;
            a[0, 0] = cell.empty;
            a[0, 1] = cell.empty;
            a[0, 2] = cell.empty;
            a[1, 0] = cell.empty;
            a[1, 1] = cell.empty;
            a[1, 2] = cell.empty;
            a[2, 0] = cell.empty;
            a[2, 1] = cell.empty;
            a[2, 2] = cell.empty;
            while (playGame)
            {
                step++;

                Console.Clear();
                Console.WriteLine("Крестики нолики");

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        switch (a[i, j])
                        {
                            case cell.krest:
                                Console.Write("X");
                                break;
                            case cell.zero:
                                Console.Write("0");
                                break;
                            case cell.empty:
                                Console.Write("-");
                                break;
                        }
                    }
                    Console.WriteLine();
                }

                if (a[0, 0] == a[0, 1] && a[0, 0] == a[0, 2] && a[0, 0] != cell.empty && a[0, 0] == cell.krest)
                {
                    Console.WriteLine("Победил крестик");
                    break;
                }
                else if (a[0, 0] == a[0, 1] && a[0, 0] == a[0, 2] && a[0, 0] != cell.empty && a[0, 0] == cell.zero)
                {
                    Console.WriteLine("Победил нолик");
                    break;
                }
                else if (a[0, 0] == a[1, 1] && a[1, 1] == a[2, 2] && a[0, 0] != cell.empty && a[0, 0] == cell.krest)

                {
                    Console.WriteLine("Победил крестик");
                    break;
                }
                else if (a[0, 0] == a[1, 1] && a[1, 1] == a[2, 2] && a[0, 0] != cell.empty && a[0, 0] == cell.zero)

                {
                    Console.WriteLine("Победил нолик");
                    break;
                }
                else if (a[0, 0] == a[0, 2] && a[0, 2] == a[2, 2] && a[0, 0] != cell.empty && a[0, 0] == cell.krest)
                {
                    Console.WriteLine("Победил крестик");
                    break;
                }
                else if (a[0, 0] == a[0, 2] && a[0, 2] == a[2, 2] && a[0, 0] != cell.empty && a[0, 0] == cell.zero)
                {
                    Console.WriteLine("Победил нолик");
                    break;
                }
                else if (a[0, 1] == a[1, 1] && a[1, 1] == a[2, 1] && a[0, 0] != cell.empty && a[0, 1] == cell.krest)
                {
                    Console.WriteLine("Победил крестик");
                    break;
                }
                else if (a[0, 1] == a[1, 1] && a[1, 1] == a[2, 1] && a[0, 0] != cell.empty && a[0, 1] == cell.zero)
                {
                    Console.WriteLine("Победил нолик");
                    break;
                }
                else if (a[0, 2] == a[1, 2] && a[1, 2] == a[2, 2] && a[0, 0] != cell.empty && a[0, 2] == cell.krest)
                {
                    Console.WriteLine("Победил крестик");
                    break;
                }
                else if (a[0, 2] == a[1, 2] && a[1, 2] == a[2, 2] && a[0, 0] != cell.empty && a[0, 2] == cell.zero)
                {
                    Console.WriteLine("Победил нолик");
                    break;
                }
                else if (a[0, 2] == a[1, 1] && a[1, 1] == a[2, 0] && a[0, 0] != cell.empty && a[0, 2] == cell.krest)
                {
                    Console.WriteLine("Победил крестик");
                    break;
                }
                else if (a[0, 2] == a[1, 1] && a[1, 1] == a[2, 0] && a[0, 0] != cell.empty && a[0, 2] == cell.zero)
                {
                    Console.WriteLine("Победил нолик");
                    break;
                }
                else if (a[1, 0] == a[1, 1] && a[1, 1] == a[1, 2] && a[0, 0] != cell.empty && a[1, 0] == cell.krest)
                {
                    Console.WriteLine("Победил крестик");
                    break;
                }
                else if (a[1, 0] == a[1, 1] && a[1, 1] == a[1, 2] && a[0, 0] != cell.empty && a[1, 0] == cell.zero)
                {
                    Console.WriteLine("Победил нолик");
                    break;
                }
                else if (a[2, 0] == a[2, 1] && a[2, 1] == a[2, 2] && a[0, 0] != cell.empty && a[2, 0] == cell.krest)
                {
                    Console.WriteLine("Победил крестик");
                    break;
                }
                else if (a[2, 0] == a[2, 1] && a[2, 1] == a[2, 2] && a[0, 0] != cell.empty && a[2, 0] == cell.zero)
                {
                    Console.WriteLine("Победил нолик");
                    break;
                }

                if (step % 2 == 1)
                {
                    Console.WriteLine("Введите номер строки");
                    stroka = int.Parse(Console.ReadLine()) - 1;
                    Console.WriteLine("Введите номер столбца");
                    stolbec = int.Parse(Console.ReadLine()) - 1;
                    if (a[stroka, stolbec] == cell.empty)
                    {
                        a[stroka, stolbec] = cell.krest;
                    }
                    else
                    {
                        step--;
                        continue;
                    }
                }
                if (step % 2 == 0)
                {
                    Console.WriteLine("Введите номер строки");
                    stroka = int.Parse(Console.ReadLine()) - 1;
                    Console.WriteLine("Введите номер столбца");
                    stolbec = int.Parse(Console.ReadLine()) - 1;
                    if (a[stroka, stolbec] == cell.empty)
                    {
                        a[stroka, stolbec] = cell.zero;
                    }
                    else
                    {
                        step--;
                        continue;
                    }
                }


                if (step > 9)
                {
                    playGame = false;
                }
            }


            Console.ReadKey();
        }
    }
}
